# Clustering Wikipedia #

To start with, I downloaded a giant backup of Wikipedia and got some tools to parse it for me.

First, I had to get a list of words by their [inverse document frequency](https://en.wikipedia.org/wiki/Tf�idf); a measure of importance. I reduced the large amount of words present to the top 5000 by this metric. I had a list of [stop words](https://en.wikipedia.org/wiki/Stop_words) once I saw the sheer amount of garbage and Wiki-specific terms (templates, etc.) that made it through. But just having the words by their importance isn't good enough, as words have multiple forms (go, going, went, etc.). Therefore, there is code to use a [lemmatizer](https://en.wikipedia.org/wiki/Lemmatisation), which works a bit better than a [stemmer](https://en.wikipedia.org/wiki/Stemming).

After all this preprocessing, the clustering can begin. As this was done for a neural networks class, I went with the [Self Organizing Map](https://en.wikipedia.org/wiki/Self-organizing_map), which works like k-means clustering in practice. The distance function, traditionally euclidean, in a self organizing map can be done by [cosine similarity](https://en.wikipedia.org/wiki/Cosine_similarity) -- I did so to good effect.

The code for the regular SOM goes on and on and on about hexagons. That's because I represented the network using a [u-matrix](http://users.ics.aalto.fi/jhollmen/dippa/node24.html). This isn't strictly necessary, but it is something of a standard.

I don't guarantee the [GSOM](https://en.wikipedia.org/wiki/Growing_self-organizing_map) variant code is bug free, as it did poorly and I'm not quite sure why. SOM itself did well.